package com.google.gwt.sample.hello.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("rpc")
public interface MyEmailService extends RemoteService {
  String sendEmail(String msg);
}