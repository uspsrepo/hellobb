package com.google.gwt.sample.hello.client;

import com.google.codemirror2_gwt.client.CodeMirrorConfig;
import com.google.codemirror2_gwt.client.CodeMirrorWrapper;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextArea;

import com.axeiya.gwtckeditor.client.CKConfig;
import com.axeiya.gwtckeditor.client.CKEditor;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.InitializeEvent;
import com.google.gwt.event.logical.shared.InitializeHandler;
import com.google.gwt.sample.hello.client.event.logical.SaveEvent;
import com.google.gwt.sample.hello.client.event.logical.SaveHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * HelloWorld application.
 */
public class Hello implements EntryPoint {
	Button b;
    private MyEmailServiceAsync myEmailService = (MyEmailServiceAsync) GWT.create(MyEmailService.class);
    ServiceDefTarget endpoint = ((ServiceDefTarget) myEmailService);

	private native String test(String msg)/*-{
  $wnd.alert(msg);
  var tx=$wnd.myFunction(1);
  //$wnd.alert(tx);
  return tx;
  }-*/;
	
	private native String dir(String msg) /*-{
//        $wnd.myRequestFs();
  $wnd.alert(msg);
  var tx=$wnd.myFunction2(2);
  //$wnd.alert(tx);
  return tx;
 	}-*/;
	
  public void onModuleLoad() {
	 // Window.alert(test("hello 0"));
	Window.alert("protocol: "+Window.Location.getProtocol()+", port: "+Window.Location.getPort()+", host: "+Window.Location.getHref());
    b = new Button("Click sendEmail", new ClickHandler() {
      public void onClick(ClickEvent event) {
        //Window.alert("Hello, AJAX 123456...");
        //Window.alert("Bye, AJAX 3...");
       // Window.alert(test("hello 1"));
        //test("1");
    	//  Window.alert(dir("123"));
        //Window.alert("Bye, AJAX 2...");
        //Window.alert(test("hello 2"));
        //Window.alert("Bye, AJAX 1...");
    	sendEmail("hi");
      }
    });
    RootPanel.get().add(b);
  }

  void sendEmail (String message) {
	  //endpoint.setServiceEntryPoint(GWT.getModuleBaseURL() + "rpc");
      myEmailService.sendEmail(message, new AsyncCallback<String>() {

        public void onFailure(Throwable caught) {
          Window.alert("RPC to sendEmail() failed.");
        }

        public void onSuccess(String result) {
          Window.alert("RPC succeeded");
          b.setText(result);
        }
      });
  }

	private CodeMirror editor;
	private CodeMirrorConfiguration config = new CodeMirrorConfiguration();
    private String JAVA_CODE = "public class HelloWorld {\n\n"+
            "       /**\n"+
            "        * @param args\n"+
            "        */\n"+
            "       public static void main(String[] args) {\n"+
            "               System.out.println(\"Hello World!\");\n"+
            "       }\n"+
            "}";
	private String HTML_CODE = "<HTML><HEAD><TITLE>Apache Tomcat Examples</TITLE>\n<META http-equiv=Content-Type content='text/html'>\n</HEAD>\n<BODY>\n<P>\n<H3>Apache Tomcat Examples</H3>\n<P></P>\n<ul>\n<li><a href='servlets'>Servlets examples</a></li>\n<li><a href='jsp'>JSP Examples</a></li>\n<li><a href='websocket'>WebSocket Examples</a></li>\n</ul>\n</BODY></HTML>";

	// set up logging to catch exceptions in browser
	public void onModuleLoad2() {

		config.setLineNumbers(true);
		config.setContinuousScanning(0);
		config.setTextWrapping(false);
		config.setAutoMatchParens(false);
		
		//config.setStyleSheetURL(GWT.getModuleBaseURL() + "/css/test.css");
		
		editor = new CodeMirror(config);
		editor.setHeight("800px");
		editor.setWidth("800px");
		
		editor.addInitializeHandler(new InitializeHandler() {
			public void onInitialize(InitializeEvent event) {
				//editor.setParser(CodeMirror.PARSER_JAVASCRPIPT);
				editor.setParser(CodeMirror.PARSER_HTML_MIXED);
				editor.setLineNumbers(true);
				//editor.setTextWrapping(true);
				editor.setIndentUnit(4);
				//editor.setStylesheetURL(GWT.getModuleBaseURL() + "/css/test.css");
				editor.setFocus();
				//editor.setSelection("body { \nmargin: 0px; \n}");
				editor.reindent();
				editor.setContent(HTML_CODE);
			}
		});
		
		editor.addSaveHandler(new SaveHandler() {

			public void onSave(SaveEvent event) {
				Window.alert("saved!");
			}
		});
		
		RootPanel.get().add(editor);
	
	}

	        @SuppressWarnings("unused")
	        public void onModuleLoad3() {
	                
	                //The GWT text area to bind with code mirror 2
	                TextArea textArea = new TextArea();

	                String JAVA_CODE = "public class HelloWorld {\n\n"+
	                                "       /**\n"+
	                                "        * @param args\n"+
	                                "        */\n"+
	                                "       public static void main(String[] args) {\n"+
	                                "               System.out.println(\"Hello World!\");\n"+
	                                "       }\n"+
	                                "}";
	                textArea.setValue(JAVA_CODE);
	                
	                RootPanel.get("gwt-content").add(textArea);

	                //we create a configuration for code mirror
	                CodeMirrorConfig config = CodeMirrorConfig.makeBuilder();
	                config = config.setMode("text/x-java").setShowLineNumbers(true).setMatchBrackets(true);

	                //then we bind code mirror to the text area
	                CodeMirrorWrapper wrapper = CodeMirrorWrapper.createEditor(textArea.getElement(), config);
					//Window.alert("OK!");
	        }
}

