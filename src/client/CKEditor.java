package com.google.gwt.sample.hello.client;

import java.util.Map;
import java.util.Set;

/**
* Server side component for the VCKEditorTextField widget. 
*/
/*
public class CKEditorTextField extends AbstractField 
implements FieldEvents.BlurNotifier, FieldEvents.FocusNotifier {

private static final long serialVersionUID = -7046683247580097506L;

private CKEditorConfig config;
private String version = "unknown";
private String insertText = null;
private String insertHtml = null;
private boolean protectedBody = false;
private boolean viewWithoutEditor = false;

public CKEditorTextField() {
super.setValue("");
setWidth(100,UNITS_PERCENTAGE);
setHeight(300,UNITS_PIXELS);
}

public CKEditorTextField(CKEditorConfig config) {
this();
setConfig(config);
}

public void setConfig(CKEditorConfig config) {
this.config = config;
}

public String getVersion() {
return version;
}

@Override
public void setValue(Object newValue) throws Property.ReadOnlyException, Property.ConversionException {
if ( newValue == null )
newValue = "";
super.setValue(newValue.toString(), false);
requestRepaint();
}

@Override
public void paintContent(PaintTarget target) throws PaintException {
super.paintContent(target);

Object currValueObject = getValue();
String currValue = currValueObject == null ? "" : currValueObject.toString();
target.addVariable(this, VCKEditorTextField.VAR_TEXT, currValue);

target.addAttribute(VCKEditorTextField.ATTR_READONLY, isReadOnly());
target.addAttribute(VCKEditorTextField.ATTR_VIEW_WITHOUT_EDITOR, isViewWithoutEditor());
//System.out.println("*** TRACE paintContent() - sending value to browser (" + currValue.length() + ") >>>" + currValue + "<<<");

if (config != null) {
target.addAttribute(VCKEditorTextField.ATTR_INPAGECONFIG, config.getInPageConfig());

if ( config.hasWriterRules() ) {
int i = 0;
Set<String> tagNameSet = config.getWriterRulesTagNames();
for( String tagName : tagNameSet ) {
target.addAttribute(VCKEditorTextField.ATTR_WRITERRULES_TAGNAME+i, tagName);
target.addAttribute(VCKEditorTextField.ATTR_WRITERRULES_JSRULE+i, config.getWriterRuleByTagName(tagName));
++i;
}
}

if ( config.hasWriterIndentationChars() ) {
target.addAttribute(VCKEditorTextField.ATTR_WRITER_INDENTATIONCHARS, config.getWriterIndentationChars());
}

if ( config.hasProtectedSource() ) {
int i = 0;
for( String protectedSourceRegex : config.getProtectedSource() ) {
target.addAttribute(VCKEditorTextField.ATTR_PROTECTED_SOURCE+i, protectedSourceRegex);
++i;
}
}
}

target.addAttribute(VCKEditorTextField.ATTR_PROTECTED_BODY, protectedBody);

if (insertHtml != null) {
target.addAttribute(VCKEditorTextField.ATTR_INSERT_HTML, insertHtml);
insertHtml = null;
}
if (insertText != null) {
target.addAttribute(VCKEditorTextField.ATTR_INSERT_TEXT, insertText);
insertText = null;
} 
}

@Override
public void changeVariables(Object source, Map<String, Object> variables) {
super.changeVariables(source, variables);

// Sets the CKEditor version
if (variables.containsKey(VCKEditorTextField.VAR_VERSION)) {
version = (String)variables.get(VCKEditorTextField.VAR_VERSION);
}

if (variables.containsKey(FocusEvent.EVENT_ID)) {
//System.out.println("*** TRACE changeVariables() - FOCUS");
fireEvent(new FocusEvent(this));
}
if (variables.containsKey(BlurEvent.EVENT_ID)) {
//System.out.println("*** TRACE changeVariables() - BLUR");
fireEvent(new BlurEvent(this));
}

// Sets the text
if (variables.containsKey(VCKEditorTextField.VAR_TEXT) && ! isReadOnly()) {
// Only do the setting if the string representation of the value has been updated
Object newVarTextObject = variables.get(VCKEditorTextField.VAR_TEXT);
String newValue = newVarTextObject == null ? "" : newVarTextObject.toString();

Object currValueObject = getValue();
final String oldValue = currValueObject == null ? "" : currValueObject.toString();
if ( ! newValue.equals(oldValue) ) {
//System.out.println("*** TRACE changeVariables() - new value (" + newValue.length() + ") >>>" + newValue + "<<<");
setValue(newValue, true);
}
}
}


@Override
public Class<?> getType() {
return String.class;
}

@Override
public void addListener(BlurListener listener) {
addListener(BlurEvent.EVENT_ID, BlurEvent.class, listener,
BlurListener.blurMethod);
}

@Override
public void removeListener(BlurListener listener) {
removeListener(BlurEvent.EVENT_ID, BlurEvent.class, listener);
}

@Override
public void addListener(FocusListener listener) {
addListener(FocusEvent.EVENT_ID, FocusEvent.class, listener,
FocusListener.focusMethod);
}

@Override
public void removeListener(FocusListener listener) {
removeListener(FocusEvent.EVENT_ID, FocusEvent.class, listener);
}

@Override
public void setHeight(float height, int unit) {
super.setHeight(height,unit);
}
@Override
public void setHeight(String height) {
super.setHeight(height);
}

@Override
public void detach() {
super.detach();
}

public boolean isViewWithoutEditor() {
return viewWithoutEditor;
}
public void setViewWithoutEditor(boolean v) {
viewWithoutEditor = v;
requestRepaint();
}

public void insertHtml(String html) {
if (insertHtml == null) 
insertHtml = html;
else 
insertHtml += html;
requestRepaint();
}

public void insertText(String text) {
if (insertText == null) 
insertText = text;
else 
insertText += text;
requestRepaint();
}

public void setProtectedBody(boolean protectedBody) {
this.protectedBody = protectedBody;
requestRepaint();
}

public boolean isProtectedBody() {
return protectedBody;
}
}
*/