package com.google.gwt.sample.hello.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.RemoteService;

public interface MyEmailServiceAsync {
	  void sendEmail(String msg,
	      AsyncCallback<String> asyncCallback);
	}